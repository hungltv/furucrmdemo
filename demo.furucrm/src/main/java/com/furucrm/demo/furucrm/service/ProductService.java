package com.furucrm.demo.furucrm.service;

import java.util.List;

import com.furucrm.demo.furucrm.entity.Product;

public interface ProductService {
	
	public List<Product> listProduct();
	public Product findById(Long id);
	public Product save(Product product);
	public void update(Product product);
	public void delete(Long id);
	public List<Product> search(String keyword);
	
}
