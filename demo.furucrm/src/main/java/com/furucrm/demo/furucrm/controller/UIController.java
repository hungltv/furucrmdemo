package com.furucrm.demo.furucrm.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.furucrm.demo.furucrm.entity.MyUploadForm;
import com.furucrm.demo.furucrm.entity.Product;
import com.furucrm.demo.furucrm.service.ProductService;

@RestController
public class UIController {

	@Autowired
	private ProductService service;
	
	@Autowired
	public void setService(ProductService service) {
		this.service = service;
	}
	private static final String IMAGES_FOLDER = "./src/main/resources/static/images/";

	private static final Logger LOGGER = LoggerFactory.getLogger(UIController.class);

	private static final String[] contructor = new String[] {".jpg", ".png"};
	
	private List<String> allowed = Arrays.asList(contructor);
	
	
	public List<Product> getAllProduct(){
		return (List<Product>) service.listProduct();
	}
	
	public Product getProduct(Long id){
		return service.findById(id);
	}
	
	@GetMapping("/login")
	public ModelAndView loginView() {
		return new ModelAndView("/login");
	}
	
	
	@GetMapping("/productAll")
	public ModelAndView getProductAll(Model model, HttpServletRequest request) {
		request.getSession().setAttribute("product", null);
		
		return new ModelAndView("redirect:/product/page/1");
	}
	
	
	@GetMapping("/searchProduct")
	public ModelAndView searchKeyword(Model model, HttpServletRequest request) {
		String keyword = request.getParameter("keyword");
		request.getSession().setAttribute("product", null);
		
		return new ModelAndView("redirect:/product/page/1?keyword=" + keyword);
	}
	
	
	@GetMapping("/product/page/{pageNumber}")
	public ModelAndView showProductPage(HttpServletRequest request, 
			@PathVariable int pageNumber, Model model) {
		PagedListHolder<?> pages = (PagedListHolder<?>) request.getSession().getAttribute("product");
		int pagesize = 10;
		
		String keyword = request.getParameter("keyword");
		List<Product> list = new ArrayList<Product>();
		if(Strings.isNotBlank(keyword)) {
			list = service.search(keyword);
		} else {
			list = service.listProduct();
		}
		
		if (pages == null) {
			pages = new PagedListHolder<>(list);
			pages.setPageSize(pagesize);
		} else {
			final int goToPage = pageNumber - 1;
			if (goToPage <= pages.getPageCount() && goToPage >= 0) {
				pages.setPage(goToPage);
			}
		}
		request.getSession().setAttribute("product", pages);
		int current = pages.getPage() + 1;
		int begin = Math.max(1, current - list.size());
		int end = Math.min(begin + 5, pages.getPageCount());
		int totalPageCount = pages.getPageCount();
		String baseUrl = "/product/page/";

		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);
		model.addAttribute("totalPageCount", totalPageCount);
		model.addAttribute("baseUrl", baseUrl);
		model.addAttribute("products", pages);
		model.addAttribute("keyword", keyword);

		return new ModelAndView("/productAll");
	}
	
	/**end phan trang*/
	

	@GetMapping("/product-detail/{id}")
	public ModelAndView getProductDetail(Model model, @PathVariable Long id ) {
		
		Product product = service.findById(id);
		model.addAttribute("product", product);
		model.addAttribute("lstProduct", getAllProduct());
		return new ModelAndView("/productDetail");
	}
	
	
	@GetMapping("/delete/{id}")
	public ModelAndView delete(Model model, @PathVariable Long id) {
		service.delete(id);
		return new ModelAndView("redirect:/productAll");
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView viewEdit(Model model, @PathVariable Long id) {
		Product product = getProduct(id);
		model.addAttribute("product", product);
		model.addAttribute("myUploadForm", new MyUploadForm());
		
		return new ModelAndView("/editProduct");
	}
	
	@PostMapping("/updateProduct")
	public ModelAndView updateProduct(Model model, Product product, MyUploadForm myUploadForm) {
		
		
		boolean ret = uploadFile(myUploadForm, model, IMAGES_FOLDER);
		if(ret) {
			if(Strings.isNotBlank((String) model.getAttribute("fileName"))) {
				product.setImage("images/" + model.getAttribute("fileName"));
			}
		}else {
			return new ModelAndView("/editProduct");
		}
		
		try {
			service.update(product);
		} catch (DataAccessException e) {
			LOGGER.error(e.getCause().getMessage());
		}
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return new ModelAndView("redirect:/productAll");
	}
	
	
	@GetMapping("/addProduct")
	public ModelAndView addNewProductView(Model model) {
		
		model.addAttribute("product", new Product());
		model.addAttribute("myUploadForm", new MyUploadForm());
		
		return new ModelAndView("/addProduct");
	}
	
	@PostMapping("/saveProduct")
	public ModelAndView saveNewProduct(Model model, MyUploadForm myUploadForm, Product product) throws Exception{
		
		boolean ret = uploadFile(myUploadForm, model, IMAGES_FOLDER);
		if(ret) {
			if(Strings.isNotBlank((String) model.getAttribute("fileName"))) {
				product.setImage("images/" + model.getAttribute("fileName"));
			}
		}else {
			return new ModelAndView("/addProduct");
		}
		
		try {
			product.setId(Long.parseLong("0"));
			service.save(product);
		} catch (DataAccessException e) {
			LOGGER.error(e.getCause().getMessage());
		}
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/productAll");
	}
	
	private boolean checkFile(String file) {
		if(Strings.isNotEmpty(file)) {
			String format = file.substring(file.indexOf("."), file.length());
			if(!allowed.contains(format)) {
				return false;
			}
		}
	
		return true;
	} 
	
	public boolean uploadFile(MyUploadForm myUploadForm, Model model, String location) {
	   	
   		MultipartFile file = myUploadForm.getFileDatas();
   		try {
   			String getFm = (String) file.getOriginalFilename();
   	   		
   			boolean check = checkFile(getFm);
   			if(!check) {
   				model.addAttribute("message", "Format file is not valid ! incluing (jpg, png)");
   				return false;
   			}
		} catch (NullPointerException e) {
			LOGGER.error(e.getCause().getMessage());
		}
   		
		
	   	if (file.isEmpty()) {
	   		return true;
	       }
	       try {
	
	           // Get the file and save it somewhere
	           byte[] bytes = file.getBytes();
	           Path path = Paths.get(location + file.getOriginalFilename());
	           Files.write(path, bytes);
	           model.addAttribute("message",
	                   "You successfully uploaded '" + file.getOriginalFilename() + "'");
	           
	           model.addAttribute("fileName", file.getOriginalFilename());
	       } catch (IOException e) {
	           e.printStackTrace();
	           return false;
	       }
			
	   		return true;
	   }
}
