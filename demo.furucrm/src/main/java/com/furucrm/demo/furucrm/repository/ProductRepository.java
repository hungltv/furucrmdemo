package com.furucrm.demo.furucrm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.furucrm.demo.furucrm.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

	@Query(value = "SELECT * FROM Product order by id DESC", nativeQuery = true) 
	public List<Product> findAll();
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE Product pr SET"
			+ " pr.description =:description,"
			+ " pr.detail =:detail,"
			+ " pr.image =:image,"
			+ " pr.product_name =:productName"
			+ " WHERE pr.id = :id", nativeQuery = true)
	
	public void updateProductById(@Param("id") Long id, 
			@Param("description") String description, 
			@Param("detail") String detail, 
			@Param("image") String image,
			@Param("productName") String productName);
	
	@Query(value = "SELECT * FROM Product pr where pr.product_name LIKE %:keyword% order by id DESC", nativeQuery = true) 
	public List<Product> search(@Param("keyword") String keyword);
}
