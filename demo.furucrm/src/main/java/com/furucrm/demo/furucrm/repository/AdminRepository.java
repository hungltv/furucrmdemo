package com.furucrm.demo.furucrm.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.furucrm.demo.furucrm.entity.Admin;

public interface AdminRepository extends CrudRepository<Admin, Long> {

	
	@Query(value = "SELECT * FROM Admin ad where ad.username =:username", nativeQuery = true) 
	Admin getByUserName(@Param("username") String username);
	
	
}
