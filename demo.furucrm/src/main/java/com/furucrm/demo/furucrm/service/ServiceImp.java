package com.furucrm.demo.furucrm.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.furucrm.demo.furucrm.entity.Product;
import com.furucrm.demo.furucrm.repository.ProductRepository;

@Service
@Repository
public class ServiceImp implements ProductService {
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	ProductRepository productRepository;

	@Override
	public List<Product> listProduct() {
		return (List<Product>) productRepository.findAll();
	}

	@Override
	public Product findById(Long id) {
		
		Optional<Product> ret = productRepository.findById(id);
		
		return ret.get();
	}

	@Override
	public Product save(Product product) {
		return productRepository.save(product);
	}

	@Override
	public void update(Product product) {
		String description = product.getDescription();
		String detail = product.getDetail();
		Long id = product.getId();
		String image = product.getImage();
		String product_name = product.getProductName();
		productRepository.updateProductById(id, description, detail, image, product_name);
	}

	@Override
	public void delete(Long id) {
		productRepository.deleteById(id);
	}

	@Override
	public List<Product> search(String keyword) {
		
		return productRepository.search(keyword);
	}

	
}
